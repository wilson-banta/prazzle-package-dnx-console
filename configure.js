var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var extend = require('util')._extend;

module.exports.get = function(options, cb)
{
	var keys =  options.fn.envHelper.read('.env.required');

	options.log.debug('Read required keys:', keys);

	var existing = options.fn.envHelper.read('.env') ;

	var result = {};

	for(var k in keys){
		result[k] = existing[k] || '';
	}
	
	return cb(null, result);
}

module.exports.set = function(options, cb)
{
	options.log.debug('Setting Configuration: ', options.envSettings);
	options.fn.envHelper.write('.env', options.envSettings);
}
