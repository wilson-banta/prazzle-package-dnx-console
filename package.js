var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');


module.exports = function(options, cb)
{
	options.log.info('Packing Dnx Console App To: %s', options.outputDir);

	async.waterfall([
		async.apply(options.dnxConsoleFn.gitVersionDirectory, options),
		cleanDir,
		runDnuPublish,
		updateProjectJsonInPublishDir,
		getRequiredEnvironmentKeys,
		zipDirectory,
		writeEnvironmentKeys
	], cb);
}



function cleanDir(options,cb){
	options.fn.cleanDir(options.outputDir, function(err){ cb(err,options); })
}

function runDnuPublish(options, cb){

	var dnu_publish = spawn('dnu.cmd', ['publish', '-o', options.tmpDir, '--no-source', '--configuration', 'Release' ]);
	var hasError;

	dnu_publish.stdout.on('data', (data) => {
	  options.log.debug(data.toString());
	});

	dnu_publish.stderr.on('data', (data) => {
		hasError = true;
	  options.log.error(data.toString());
	});

	dnu_publish.on('close', (code) => {

		if(hasError) return callback('An error occured during the dnu publish command.');

		options.log.info('DNU publish completed');

		return cb(null, options);
	});
}

function updateProjectJsonInPublishDir(options,cb)
{
	fs.writeFileSync(path.join(options.tmpDir,'project.json'), JSON.stringify(options.packageJson));
	cb(null, options);
}

function getRequiredEnvironmentKeys(options, cb)
{
	options.log.verbose('Reading environment keys...');

	options.environmentKeys = options.fn.envHelper.read('.env.required');

	cb(null, options);
}

function writeEnvironmentKeys(options, cb){

	options.log.verbose('Writing environment keys...');

	var outputFilename = options.dnxConsoleFn.packageEnvFilepath(options);

	console.log(outputFilename);
	options.fn.envHelper.writeKeys(outputFilename, options.environmentKeys);

	cb(null, options);
}


function zipDirectory(options, cb){
	var outputFilename = options.dnxConsoleFn.packageFilepath(options);

	if(fs.existsSync(outputFilename)) fs.unlinkSync(outputFilename);

	options.fn.zipHelper.zipDir(options.tmpDir, outputFilename, function(e, r){ cb(e, options) });
}

//--
//return options.fn.readEnvZipAndUpload({version: version}, cb);
