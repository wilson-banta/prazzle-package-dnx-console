var path = require('path');
var fs = require('fs');

var async = require('async');
var extend = require('util')._extend;


/*
Copy

*/
exports.package = function(options, cb){
	require('./package.js')(prepareOptions(options), cb);
}


exports.publish = function(options, cb){
	require('./publish.js')(prepareOptions(options), cb);
}

exports.configure = {};
exports.configure.get = function(options, cb){
	require('./configure.js').get(prepareOptions(options), cb);
}
exports.configure.set = function(options, cb){
	require('./configure.js').set(prepareOptions(options), cb);
}


exports.publish = function(options, cb){

	require('./publish.js')(prepareOptions(options), cb);
}


exports.install = function(options, cb){
	cb();
}

function packageFilepath(options){
	return path.join(options.outputDir, packageFilename(options, options) );
}

function packageFilename(options){
	return [options.project, '-', options.version, '.zip'].join('');
}


function packageEnvFilepath(options){
	return path.join(options.outputDir,   [options.project, '-', options.version, '.env'].join('') );
}


function readPackageJson(){
	var projectJsonPath = path.resolve('project.json');
	if(!fs.existsSync(projectJsonPath)) throw "Can't find project.json. Looked here: " + projectJsonPath;
	return require(projectJsonPath);
}


function gitVersionDnxConsole(options, cb){
	options.fn.gitVersion(options.packageJson.version, function(error, v){

		if(error) return  cb(error);

		options.version = v;

		options.log.info('Git Version Calculated: %s', options.version);

		cb(null, options);
	});
}


function prepareOptions(options){
	var publishOptions  = extend({}, options);

	publishOptions.packageJson = readPackageJson();
	publishOptions.outputDir = options.outputDir || ['..','dist', options.project].join(path.sep);

	publishOptions.dnxConsoleFn = {
		packageFilepath:  packageFilepath,
		packageFilename: packageFilename,
		packageEnvFilepath: packageEnvFilepath,
		gitVersionDirectory: gitVersionDnxConsole,
	};

	return publishOptions;
}
